/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MS5611_H
#define MS5611_H

#include "WProgram.h"

//Address 
#define MS5611_ADDRESS  0x77   

// Command Registers
#define MS5611_D1 0x40
#define MS5611_D2 0x50
#define MS5611_ADC 0x00
#define MS5611_RESET 0x1E
#define MS5611_PROM 0xA2

// OSR constants
#define MS5611_OSR_256 0x00
#define MS5611_OSR_512 0x02
#define MS5611_OSR_1024 0x04
#define MS5611_OSR_2048 0x06
#define MS5611_OSR_4096 0x08

// command delay
#define MS5611_COMMAND_DELAY 10

// used for testing
typedef struct
{
   uint32_t rawPressure;
   uint32_t rawTemperature;
   int32_t deltaTemp;
   uint32_t pressure;
   uint32_t basePressure;
   float temperature;
   float altitude;
} ms5611_data_t;


class MS5611
{
  public:
    void setup(uint8_t osr);
    void reset();
    float getTemperature();
    uint32_t getPressure(boolean comp=true);
    void setBasePressure(uint32_t pressure);
    uint32_t getBasePressure();
    float convertToAltitude(uint32_t pressure, float heightUnits);
    ms5611_data_t test();
  private:
    uint32_t _basePressure;
    uint16_t _C[6];
    uint8_t _osr;
    void readPROM();
    uint32_t getRawPressure(uint8_t osr);
    uint32_t getRawTemperature(uint8_t osr);
    int32_t getDeltaTemp(uint32_t rawTemp);
    void startConversion(uint8_t d, uint8_t osr);
    uint32_t readADC();
    void writeCommand(uint8_t register);
    uint16_t read16bit(uint8_t register);
    uint32_t read24bit(uint8_t register);
};


#endif /*MS5611_H*/
