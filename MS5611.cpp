/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MS5611.h"
#include <Wire.h>

extern boolean DEBUG;

void MS5611::setup(uint8_t osr)
{
  _osr = osr;
  reset();
  readPROM();
  setBasePressure(getPressure());
}

 void MS5611::reset()
 {
   writeCommand(MS5611_RESET);
   delay(500);
 }
 
 void MS5611::setBasePressure(uint32_t pressure)
 {
   _basePressure = pressure;
 }
 
 uint32_t MS5611::getBasePressure()
 {
   return _basePressure;
 }
 
 uint32_t MS5611::getPressure(boolean comp)
 {
  uint32_t rawTemp = getRawTemperature(_osr);
  int32_t deltaTemp = getDeltaTemp(rawTemp);
  uint32_t rawPressure = getRawPressure(_osr);
     
  int64_t off  = ((int64_t)_C[1] <<16) + (((int64_t)deltaTemp * _C[3]) >> 7);
  int64_t sens = ((int64_t)_C[0] <<15) + (((int64_t)deltaTemp * _C[2]) >> 8);
  
  if (comp)
  {
    int32_t temp = getTemperature() * 100; 
    int64_t off2 = 0;
    int64_t sens2 = 0;

    if (temp < 2000)
    {
      off2 = 5 * ((temp - 2000) * (temp - 2000)) >> 1;
      sens2 = 5 * ((temp - 2000) * (temp - 2000)) >> 2;
    }

    if (temp < -1500)
    {
      off2 = off2 + 7 * ((temp + 1500) * (temp + 1500));
      sens2 = sens2 + 11 * ((temp + 1500) * (temp + 1500)) >> 1;
    }

    off = off - off2;
    sens = sens - sens2;
  }

  return ((((rawPressure * sens ) >> 21) - off) >> 15);
 }
 
 uint32_t MS5611::getRawPressure(uint8_t osr)
 {
   startConversion(MS5611_D1, osr);
   return readADC();
 }
 
 float MS5611::getTemperature()
 {
   uint32_t rawTemp = getRawTemperature(_osr);
   return (2000 + ((getDeltaTemp(rawTemp) * _C[5]) >> 23)) / 100.0;
 }
 
 int32_t MS5611::getDeltaTemp(uint32_t rawTemp)
 {
   return rawTemp - ((uint32_t)_C[4] << 8); 
 }
 
 uint32_t MS5611::getRawTemperature(uint8_t osr)
 {
   startConversion(MS5611_D2, osr);
   return readADC();
 }
 
 float MS5611::convertToAltitude(uint32_t pressure, float heightUnits) 
 {
  return heightUnits * 44330.0 * (1.0 - pow((double)pressure / (double)_basePressure, 1.0 / 5.25));
 }
 
 void MS5611::startConversion(uint8_t d, uint8_t osr)
 {
   writeCommand(d + osr);
   delay(MS5611_COMMAND_DELAY);
 }
 
 uint32_t MS5611::readADC()
 {
   return read24bit(MS5611_ADC);    
 }
 
 void MS5611::readPROM()
 {
   for (int i = 0; i < 6; i++)
   {
     // request in the 16 bit value from the PROM register
     _C[i] = read16bit(MS5611_PROM + (i*2));
     if (DEBUG)
     {
       Serial.print("_C["); Serial.print(i); Serial.print("] = "); Serial.println(_C[i]);
     }
   }
   
 }
 
 uint32_t MS5611::read24bit(uint8_t reg)
 {
    writeCommand(reg);

    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 3);
    while (Wire.available() < 1) { }
    uint8_t val[3] = {0};
    val[2] = Wire.receive();
    val[1] = Wire.receive();
    val[0] = Wire.receive();
    
    return (uint32_t)val[2] << 16 | (uint32_t)val[1] << 8 | (uint32_t)val[0];
 }
 
 uint16_t MS5611::read16bit(uint8_t reg)
 {
    writeCommand(reg);

    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 2);
    while (Wire.available() < 1) { }
    uint8_t val[2] = {0};
    val[1] = Wire.receive();
    val[0] = Wire.receive();
    
    return (uint16_t)val[1] << 8 | (uint16_t)val[0];
 }
 

 void MS5611::writeCommand(uint8_t reg)
 {
   Wire.beginTransmission(MS5611_ADDRESS);
   Wire.send(reg);
   Wire.endTransmission();
 }
 
 ms5611_data_t ms5611_data;
 
 ms5611_data_t MS5611::test()
 {
   ms5611_data.rawPressure = getRawPressure(_osr);
   ms5611_data.rawTemperature = getRawTemperature(_osr);
   ms5611_data.deltaTemp = getDeltaTemp(ms5611_data.rawTemperature);
   ms5611_data.pressure = getPressure();
   ms5611_data.basePressure = getBasePressure();
   ms5611_data.temperature = getTemperature();
   ms5611_data.altitude = convertToAltitude(ms5611_data.pressure, 1.0);
   
   Serial.println("*********");
   Serial.print("rawPressure = "); Serial.println(ms5611_data.rawPressure);
   Serial.print("rawTemperature = "); Serial.println(ms5611_data.rawTemperature);
   Serial.print("deltaTemp = "); Serial.println(ms5611_data.deltaTemp);
   Serial.print("temperature = "); Serial.println(ms5611_data.temperature);
   Serial.print("pressure = "); Serial.println(ms5611_data.pressure);
   Serial.print("basePressure = "); Serial.println(ms5611_data.basePressure);
   Serial.print("altitude = "); Serial.println(ms5611_data.altitude);
   return ms5611_data;
 }
